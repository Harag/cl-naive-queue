(defsystem "cl-naive-queue"
  :description "A simple queue based on a paper (Implementing Queues in Lisp) by Peter Norvig and Richard C. Waters . The package only implements the vector based shifting version for performance reasons. Optional thread safety was added."
  :version "2021.6.21"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-atomic)
  :components (
	       (:file "src/package")
	       (:file "src/queue" :depends-on ("src/package"))))

