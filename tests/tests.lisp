(in-package :cl-naive-queue.tests)

(testsuite  :queue
  (testcase :raw
	    :expected '(2 1 0 NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL 4 3 2 1 0)
	    :actual (let ((q (make-queue))
			  (actual))
		      (dotimes (i 17) (enqueue q '-)
			(dequeue q))
		      (dotimes (i 5)
			(enqueue q i))

		      (dotimes (i 20)
			(push (svref (cl-naive-queue::q-elements q) i) actual))

		      (reverse actual))
	    :info "Checking queue internal vector for correctness.")
  (testcase :elements
	    :expected '(0 1 2 3 4)
	    :actual (let ((q (make-queue)))

		      (dotimes (i 17) (enqueue q '-)
			(dequeue q))
		      (dotimes (i 5)
			(enqueue q i))

		      (queue-elements q))
	    :info "Testing queue-elements output."))

;;(report (run))

