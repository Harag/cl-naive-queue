(require :cl-naive-queue)
(defpackage :naive-examples (:use :cl :cl-naive-queue))
(in-package :naive-examples)

;;Simple queue test
(time
 (let ((q (make-queue)))
   (print q)
   (dotimes (i 17) (enqueue q '-)

     (dequeue q))
   (print q)
   (dotimes (i 5)
     (enqueue q i))
   q))

;;Putting some pressure on without thread safety
(time
 (let ((q (make-queue :size 1400000)))
   (dotimes (i 1000000) (enqueue q '-)
     (dequeue q))
   (dotimes (i 5)
     (enqueue q i))
   ;;q
   nil))

;;Putting some pressure on with thread safety
(time
 (let ((q (make-queue :size 1400000 :safe-p  t)))
   (dotimes (i 1000000) (enqueue q '-)
     (dequeue q))
   (dotimes (i 5)
     (enqueue q i))
   ;;q
   nil))
