(defsystem "cl-naive-queue.tests"
  :description "Tests for cl-naive-queue."
  :version "2021.6.21"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-queue :cl-naive-tests)
  :components (
	       (:file "tests/package")
	       (:file "tests/tests" :depends-on ("tests/package"))))

