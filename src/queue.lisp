(in-package :cl-naive-queue)

;;;; This queue is taken from "Implementing Queues in "Lisp"
;;;; https://3e8.org/pub/scheme/doc/lisp-pointers/v4i4/p2-norvig.pdf

;; Extract from the paper:

;;The implementation does not treat the vector as a ring. Rather,
;;whenever the queue reaches the end of the vector, it is shifted over
;;(by the function shift-queue, which also extends the vector if
;;necessary).  One might well imagine that operating on the vector as
;;a ring has to be better than shifting everything over every time the
;;queue reaches the edge of the vector. However, as long as the queue
;;is significantly shorter than the vector (say only 2/3 the length or
;;less) then shifting does not have to occur very often, and
;;performing occasional shifts ends up being cheaper than complex
;;decrementing of the pointers all of the time. Dequeue and enqueue
;;both become significantly more efficient, and dequeue becomes short
;;enough to easily code in line.

(defstruct q
  front
  end
  size
  elements
  mvar)

(defgeneric queue-elements (q) (:documentation "A fresh list of the elements in the queue."))
(defgeneric empty-queue-p (q) (:documentation "Predicate whether the queue is empty."))
(defgeneric queue-front (q) (:documentation "The first element in the queue."))
(defgeneric dequeue (q) (:documentation "Removes an element from the queue, and return it. Pre-condition: (not (empty-queue-p q))"))
(defgeneric enqueue (q element) (:documentation "Add an element to the queue."))
(defgeneric shift-queue (q) (:documentation "Shift the queues; internal method."))

(defun make-queue (&key (size 20) (safe-p nil))
  "Creates a queue.

SAFE-P indicates if thread safety should be used."
  (let ((mvar (if safe-p (cl-naive-mvar:make-mvar))))
    (if safe-p (cl-naive-mvar:put mvar 1))

    (make-q :front (- size 1) :end (- size 1) :size size
	    :elements (make-sequence 'simple-vector size)
	    :mvar mvar)))


;;TODO: What about creating a new vector with just the "active"
;;elements and returning that instead of a list?
(defmethod queue-elements ((q q))
  "Returns the queue's elements as a list."
  (do ((i (1+ (q-end q)) (1+ i))
       (result nil))
      ((> i (q-front q)) result)
    (push (svref (q-elements q) i) result)))

(defmethod empty-queue-p ((q q))
  "Tests if queue is empty."
  (= (q-front q) (q-end q)))

(defmethod queue-front ((q q))
  "Gets item at the front of queue."
  (svref (q-elements q) (q-front q)))

(defmethod dequeue ((q q))
  "Removes item from queue."
  (when (q-mvar q)
    (cl-naive-mvar:take (q-mvar q)))

  (prog1
      (svref (q-elements q) (q-front q))
    ;;Extra step intruduced by me to make sure queued items don't
    ;;dont get held on to in memory
    (setf (svref (q-elements q) (q-front q)) nil)
    (decf (q-front q))
    (when (q-mvar q)
      (cl-naive-mvar:put (q-mvar q) 1))))

(defmethod enqueue ((q q) item)
  "Adds item to queue."
  (when (q-mvar q)
    (cl-naive-mvar:take (q-mvar q)))

  (setf (svref (q-elements q) (q-end q)) item)

  (when (minusp (decf (q-end q)))
    (shift-queue q))

  (when (q-mvar q)
    (cl-naive-mvar:put (q-mvar q) 1)))

(defmethod shift-queue ((q q))
  "Shifts queue. See paper/docs."
  (let* ((elements (q-elements q))
	 (new elements))

    (when (> (q-front q) (/ (q-size q) 2))
      (setq new (make-sequence 'simple-vector (* 2 (q-size q))))
      (setf (q-elements q) new)
      (setf (q-size q) (* 2 (q-size q))))

    (setf (q-end q) (- (q-size q) 2 (q-front q)))
    (replace new elements :start1 (1+ (q-end q)))
    (setf (q-front q) (1- (q-size q)))))

