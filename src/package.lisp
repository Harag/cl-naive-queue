(in-package :common-lisp-user)

(defpackage :cl-naive-queue
  (:use :cl)
  (:export
   :make-queue
   :queue-elements
   :empty-queue-p
   :queue-front
   :dequeue
   :enqueue))
